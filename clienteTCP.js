var net = require('net');
const readline = require('readline');
const r1 = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var client = new net.Socket();
client.connect(9001, '127.0.0.1', function() {
	console.log('Connected');
});

r1.on('line',function(line){
    client.write(line)
})

client.on('close', function() {
	console.log('Connection closed');
});
