var PORT = 9090
var HOST = '127.0.0.1'; //'localhost'; //"10.100.67.101"
var net = require('net')
var easysocket = require('./easysocket.js');
var fs = require('fs');
var base64Img = require('base64-img');
var serverTCP = net.createServer(onClientConnected);
serverTCP.listen(PORT, HOST, function() {
    console.log('Servidor montado en %j', serverTCP.address());
});
let listaClientes=[]
var LProductos=[]
var ID =-1
var files = fs.readdirSync('./ImagenesDescarga');

function RandomImagen(){
    return files[RandNumber(0,files.length)]
}

function RandNumber(low,high){
    return Math.floor(Math.random() * (high - low) + low)
}

function producto(){
    var imagenPO=RandomImagen()
    var prod={
	id:genID(),
	Nombre:"1",
	Descripcion:"1",
	Precio:genRandom(100),
	Categoria:"1",
	Existencias:genRandom(100),
	Imagen:{nombre:imagenPO,contenido:base64Img.base64Sync('./ImagenesDescarga/'+imagenPO).toString()},
    Promocion:"1",
    }
    return prod
}

setInterval(pruebas,8000);
function pruebas(){
    LProductos.push(producto())
    var prod={tipo:"Lista",LP:LProductos}
    prod=JSON.stringify(prod)
    for(var socket of listaClientes){
        Envia(prod,socket)
    }
}

function actualiza(){
    var prod={tipo:"Lista",LP:LProductos}
    prod=JSON.stringify(prod)
    for(var socket of listaClientes){
        Envia(prod,socket)
    }
}

function Envia(Obj, sock) {
    Obj = JSON.stringify(Obj);
    easysocket.send(sock, Obj, function(sock) {
        console.log(`Enviado!`);
    });
}

function onClientConnected(sock) {
    listaClientes.push(sock)
    function handleMessage(socket, buffer) {
        var OSmsg = JSON.parse(buffer.toString())
        OSmsg=JSON.parse(OSmsg)
        console.log("Recibio: "+OSmsg.tipo)
        if (OSmsg.tipo === "Compra") {
            console.log("Realizando compra")
            var respuesta=Compra(OSmsg.productos,OSmsg.saldo)
            Envia(respuesta,socket)
        }
    }
    sock.on('data', function(data) {
        easysocket.recieve(sock, data, handleMessage);
    });
    sock.on('close', function() {
        console.log("Cliente desconectado")
    });
    sock.on('error', function() {
        console.log("NO");
    });
}

function genRandom(n){
    var numR= Math.floor(Math.random() * n)+1;
    return numR   
}

function genID(){
    ID=ID+1
    return ID
}

function Compra(prod,saldo){
    var saldo2 = saldo
    console.log(prod)
    var precio=0
    for(var elem of prod){
        console.log(elem)
        precio=precio+elem.Precio
        if(precio<saldo){
            if(LProductos[parseInt(elem.ID)].Existencias-elem.Cantidad<0){
                return JSON.stringify({tipo:"respuesta",msg:"No hay existencias suficientes",saldo:saldo2})
            }
        }else{
            return JSON.stringify({tipo:"respuesta",msg:"No hay recursos suficientes",saldo:saldo2})
        }
    }
    for(var elem of prod){
        LProductos[parseInt(elem.ID)].Existencias-=elem.Cantidad
        saldo2=saldo2-elem.Precio
    }
    actualiza()
    return JSON.stringify({tipo:"respuesta",msg:"OK->"+precio,saldo:saldo2})
}