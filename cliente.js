//var HOST='10.100.67.101';//"127.0.0.1"
var PORT = 5007 ;
var dgram = require('dgram');
var client = dgram.createSocket({reuseAddr:true,type:'udp4'});

client.on('listening', function () {
    var address = client.address();
    console.log('UDP Client listening on ' + address.address + ":" + address.port);
    client.setBroadcast(true)
    client.setMulticastTTL(128); 
    client.addMembership('224.0.0.1');
});

client.on('message', function (message, remote) {
    console.log('Mensaje recibido de: ' + remote.address + ':' + remote.port +' - ' + message);
});

client.bind(PORT);
