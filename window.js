var easysocket = require('./easysocket.js');
var net = require('net')
var client = new net.Socket();
var base64Img = require('base64-img');
var imagenes = new Set()
var saldo=1000
client.connect(9090,'127.0.0.1', function() {
	alert('¡Conectado a tienda!');
});
client.on('close', function() {
	alert('Connection closed');
});
function handleMessage(socket,buffer){
	var LP=JSON.parse(buffer.toString())
	LP=JSON.parse(LP)
	console.log(LP.tipo)
	if(LP.tipo==="Lista"){
		productos=LP.LP
		cargar()
	}
	if(LP.tipo==="respuesta"){
		$("#saldoC").text(LP.saldo)
		alert("Respuesta: "+LP.msg)
	}
}
var productos=[]
var carrito=new Set()
client.on('data', function(data) {
    console.log("bytes in:"+data.length);
    easysocket.recieve(client,data,handleMessage);
});

$(() => {
	$("#t2").on("click", "tr", function(e) {
		var tr = e.currentTarget;
		var td1 = tr.getElementsByTagName("td")
		var ID = $(td1[0]).html()
		for(var elem of carrito){
			if(parseInt(elem.ID)==parseInt(ID)){
				carrito.delete(elem)
			}
		}
		tr.remove()
		console.log(carrito)
	})

	$("#Cerrar").click(function() {
		$(".modal").removeClass("is-active");
	});
	$("#t1").on("click", "tr", function(e) {
        var tr = e.currentTarget;
        var td1 = tr.getElementsByTagName("td")
		var ID = $(td1[0]).html()
		cargarPantalla(ID)
		$(".modal").addClass("is-active");
	})
	$("#Agregar").click(function(){
		var button = '<button class="button" id="BorrarN">Eliminar</button>'
		var id = $("#idP").html()
		var cantidad=$("#numP").find(":selected").text()
		var precio = $("#PrecioP").html()
		$(".modal").removeClass("is-active");
		if(parseInt(cantidad)>0){
			$('#t2').append('<tr><td>'+id+'</td><td>'+$("#NombreP").html()+'</td><td>'+cantidad+'</td><td>'+precio+'</td><td>'+button+'</td></tr>');
			carrito.add({ID:id,Cantidad:parseInt(cantidad),Precio:parseInt(precio)*parseInt(cantidad)})
			calPrecio()
		}else{
			alert("No hay stock :(")
		}
	})
	$("#compra").click(function(){
		var OSmsg={tipo:"Compra",productos:Array.from(carrito),saldo:saldo}
		OSmsg=JSON.stringify(OSmsg)
		console.log(OSmsg)
		Envia(client,OSmsg)
		$("#tbody2").empty()
		$("#precioC").html(0.0)
		carrito=new Set()
	})
	$("#saldoC").text(saldo)
})
function cargar(){
	$('#tbody1').empty();
	for(var z of productos){
		$('#tbody1').append('<tr><td>'+z.id+'</td><td>'+z.Nombre+'</td><td>'+z.Existencias+'</td></tr>')
	}
}
function cargarPantalla(ID){
	for(var elem of productos){
		if(parseInt(elem.id)==parseInt(ID)){
			if(!imagenes.has(elem.Imagen.nombre)){
				guardarImagen(elem.Imagen.contenido,elem.Imagen.nombre)
				imagenes.add(elem.Imagen.nombre)
			}
			$("#idP").html(elem.id)
			$("#NombreP").html(elem.Nombre)
			$("#DescripcionP").html(elem.Descripcion)
			$("#PrecioP").html(elem.Precio)
			$("#CategoriaP").html(elem.Categoria)
			$("#ExistenciasP").html(elem.Existencias)
			console.log(elem.Imagen.nombre)
			$("#IMGP").attr('src','./Imagenes/'+elem.Imagen.nombre)
			$("#PromocionP").html(elem.Promocion)
			$("#numP").empty()
			for(var i = 1;i<=parseInt(elem.Existencias);i++){
				var o = new Option(i,i);
				$("#numP").append(o);
			}
			break
		}
	}
}

function Envia(sock, Obj) {
    Obj = JSON.stringify(Obj);
    easysocket.send(sock, Obj, function(sock) {
        console.log(`Enviado!`);
    });
}

function calPrecio(){
	var precio=0
	for(var elem of carrito){
		precio=precio+elem.Precio
	}
	$("#precioC").html(precio)
}

function guardarImagen(imageTEXT,nombre){
	var image=base64Img.imgSync(imageTEXT,"./Imagenes",RemoverExtension(nombre))
}

function RemoverExtension(nombre){
	var index = nombre.indexOf(".")
	return nombre.substring(0,index)
}